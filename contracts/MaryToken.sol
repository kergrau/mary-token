// SPDX-License-Identifier: ISC
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";

contract MaryToken is ERC721Enumerable {
    string public baseURI;
    mapping(uint256 => string) private _hashIPFS;

    constructor(
        string memory _name,
        string memory _symbol
    ) ERC721(_name, _symbol) {
        baseURI = "https://ipfs.io/ipfs/";
    }

    function mint(address _to, string memory _hash) public {
        uint256 supply = totalSupply() + 1;
        _safeMint(_to, supply);
        _hashIPFS[supply] = _hash;
    }

    function walletOfOwner(
        address _owner
    ) public view returns (uint256[] memory) {
        uint256 ownerTokenCount = balanceOf(_owner);
        uint256[] memory tokenIds = new uint256[](ownerTokenCount);
        for (uint256 i; i < ownerTokenCount; i++) {
            tokenIds[i] = tokenOfOwnerByIndex(_owner, i);
        }
        return tokenIds;
    }

    function tokenURI(
        uint256 tokenId
    ) public view virtual override returns (string memory) {
        string memory currentBaseURI = _baseURI();
        return
            (bytes(currentBaseURI).length > 0 &&
                bytes(_hashIPFS[tokenId]).length > 0)
                ? string(abi.encodePacked(currentBaseURI, _hashIPFS[tokenId]))
                : "";
    }

    function _baseURI() internal view virtual override returns (string memory) {
        return baseURI;
    }

    function setBaseURI(string memory _newBaseURI) public {
        baseURI = _newBaseURI;
    }
}
